# myprocinfo.ps1

DO {

Write-Output "`n`n
1 - Hvem er jeg og hva er navnet paa dette scriptet?
2 - Hvor lenge er det siden siste boot?
3 - Hvor mange prosesser og traader finnes?
4 - Hvor mange context switch'er fant sted siste sekund?
5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode og i usermode siste sekund?
6 - Hvor mange interrupts fant sted siste sekund?
9 - Avslutt dette scriptet"


Write-Output "`nVelg en funksjon: "
$nr = Read-Host

#Viser dato for siste boot:
#$sisteboot = (Get-CimInstance -ClassName win32_operatingsystem) | Select-Object -ExpandProperty lastbootuptime

switch ($nr) {
    1 {
        $scriptnavn = $MyInvocation.MyCommand.Name
        Write-Output "`nJeg er $env:UserName, og navnet paa scriptet er $scriptnavn"
      }
    2 {
        $sisteboot = (Get-Date) - (Get-CimInstance -ClassName win32_operatingsystem).LastBootUptime
        Write-Output "`nTid siden siste boot: $sisteboot"
      }
    3 {
        $antallprosesser = (Get-CimInstance Win32_OperatingSystem).NumberOfProcesses
        $antalltraader = (Get-Process | Select-Object -ExpandProperty Threads).Count
        Write-Output "`nAntall prosesser: $antallprosesser `nAntall traader: $antalltraader"
      }
    4 {
        $contextswitch = (Get-CimInstance Win32_PerfFormattedData_PerfOS_System).ContextSwitchesPersec
        Write-Output "`nAntall context switcher siste sekund: $contextswitch"
      }
    5 {
        $usermode = (Get-CimInstance Win32_PerfFormattedData_Counters_ProcessorInformation | Where-Object {$_.Name -eq "_Total"}).PercentUserTime
        $kernelmode = (Get-CimInstance Win32_PerfFormattedData_Counters_ProcessorInformation | Where-Object {$_.Name -eq "_Total"}).PercentPrivilegedTime
        Write-Output "`nAndel i user mode: $usermode`nAndel i kernel mode: $kernelmode"
      }
    6 {
        $interrupts = (Get-CimInstance Win32_PerfFormattedData_Counters_ProcessorInformation | Where-Object {$_.Name -eq "_Total"}).InterruptsPersec
        Write-Output "`nAntall interrupts per sekund: $interrupts"
      }
    9 { exit }
}

} while ($nr -ne 9)