# Obligatorisk oppgave 3 - PowerShell pipelines og scripting

Denne oppgaven består av de følgende laboppgavene fra kompendiet:

* 11.6.c (Prosesser og tråder)
* 12.4.c (En prosess sin bruk av virtuelt og fysisk minne)
* 13.10.b (Internminne)
* 13.10.c (Informasjon om deler av filsystemet)

SE OPPGAVETEKST I KOMPENDIET. HUSK Å REDIGER TEKSTEN NEDENFOR!

## Gruppemedlemmer
* Gjert Michael Torp Homb  
* Erlend Husbyn
* Christian Isnes

## Sjekkliste

* Har navnene på gruppemedlemmene blitt skrevet inn over?
* Har læringsassistenter og foreleser blitt lagt til med leserettigheter?
* Er issue-tracker aktivert?
* Er pipeline aktivert, og returnerer pipelinen "Successful"?
