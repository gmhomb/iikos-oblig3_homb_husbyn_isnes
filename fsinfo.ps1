#fsinfo.ps1

param([string]$p1)

$path = (Get-CimInstance Win32_LogicalDisk).DeviceID | ForEach-Object {(Get-ChildItem "$_\" -Force -Recurse -ErrorAction SilentlyContinue -Directory "$p1").FullName}

$disk = (Split-Path -Path "$path").substring(0,2)

$diskbokstav = (Get-CimInstance Win32_LogicalDisk).DeviceID | Where-Object {$_ -match "$disk"}

$total = (Get-CimInstance Win32_LogicalDisk | Where-Object {$_ -match "$diskbokstav"}).Size
$ledig = (Get-CimInstance Win32_LogicalDisk | Where-Object {$_ -match "$diskbokstav"}).Freespace
$brukt = ($total - $ledig)
$prosent = ($brukt/$total) * 100

Write-Output "Partisjonen som $p1 befinner seg paa er $prosent full"

$antallfiler = (Get-ChildItem "$path" -Recurse -File -Force | Measure-Object).Count
Write-Output "Det finnes $antallfiler filer"

$storstefil = (Get-ChildItem "$path" -Recurse -File -Force | Sort-Object Length -Descending | Select-Object -ExpandProperty Fullname -First 1)
$storrelse = (Get-ChildItem "$path" -Recurse -File -Force | Sort-Object Length -Descending | Select-Object -First 1).Length
Write-Output "Den storste er $storstefil som er $storrelse B stor."

#$totalfilstorrelse = (Get-ChildItem "$path" -Recurse -File -Force | ForEach-Object {$_.Length.Sum})

foreach ($i in (Get-ChildItem "$path" -Recurse -File -Force | Select-Object)) {
    $totalfilstorrelse += $i.Length
}

$gjennomsnittstr = ($totalfilstorrelse/$antallfiler)
Write-Output "Gjennomsnittlig filstorrelse er $gjennomsnittstr B"