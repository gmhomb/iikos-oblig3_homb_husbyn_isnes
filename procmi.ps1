#procmi.ps1

param([int]$p1, [int]$p2)

$dato = Get-Date -format "yyyyMMdd-hhmmss"

$vm1 = (Get-Process -Id $p1).VirtualMemorySize / 1Mb
$ws1 = (Get-Process -Id $p1).WorkingSet / 1Kb

Write-Output "`n******** Minne info om prosess med PID $p1 ********
Total bruk av virtuelt minne: $vm1 MB
Storrelse paa Working Set: $ws1 KB`n" | Out-File -FilePath .\$p1--$dato.meminfo


$vm2 = (Get-Process -Id $p1).VirtualMemorySize / 1Mb
$ws2 = (Get-Process -Id $p1).WorkingSet / 1Kb

Write-Output "******** Minne info om prosess med PID $p2 ********`n
Total bruk av virtuelt minne: $vm2`n
Storrelse paa Working Set: $ws2" | Out-File -FilePath .\$p2--$dato.meminfo