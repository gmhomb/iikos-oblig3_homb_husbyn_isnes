#internminne.ps1

param([string]$p1)

$minne = ((Get-Process $p1 | Measure-Object WorkingSet -Sum).Sum) / 1Mb
$totalminne = (Get-CimInstance -ClassName 'Cim_PhysicalMemory' | Measure-Object -Property Capacity -Sum).Sum / 1Mb

Write-Output "Prosessen $p1 benytter $minne MB av totalt $totalminne MB"

#Oppgave om chrome threads (så litt sent at book.pdf ikke var oppdatert)
#foreach ($i in Get-Process chrome) { Write-Host -NoNewLine $i.Name $i.Id $i.Threads.Count`n}